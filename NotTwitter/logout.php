<?php

session_start();

//People not logged in should not be allowed to go further
if (isset($_SESSION["userLoggedIn"]) == "") {
    header("Location: home.php");
    exit();
}

//Log the user out
unset($_SESSION["userLoggedIn"]);
session_unset();
session_destroy();
header("Location: home.php");
exit();