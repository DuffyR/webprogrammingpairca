<?php

require_once 'IDisplay.php';
require_once 'inc/QuickToString.php';
require_once 'User.php';

/**
 * Description of Blog
 *
 * @author D00181131
 */
class Blog implements IDisplay{
    use QuickToString;
    
    private $id;
    private $body;
    private $tags;
    private $commentsAllowed;
    private $commentCount;
    private $postDate;
    private $blogger;
    
    function __construct() {
//        $this->id = $id;
//        $this->body = $body;
//        $this->commentsAllowed = $commentsAllowed;
//        $this->commentCount = $commentCount;
//        $this->postDate = $postDate;
//        $this->user = $user;
        //$this->tags = explode(",", $this->tags);
    }

    public function getId() {
        return $this->id;
    }
    public function getBody() {
        return $this->body;
    }
    public function getTagsAsArray() {
        return explode(",", $this->tags);
    }
    public function getTagsAsDelimitedString() {
        return $this->tags;
    }
    function getCommentsAllowed() {
        return $this->commentsAllowed;
    }
    public function getCommentCount() {
        return $this->commentCount;
    }
    public function getPostDate() {
        return $this->postDate;
    }
    public function getBlogger() {
        return $this->blogger;
    }

    public function setId($id) {
        $this->id = $id;
    }
    public function setBody($body) {
        $this->body = $body;
    }
    public function setTags($tags) {
        $this->tags = $tags;
    }
    function setCommentsAllowed($commentsAllowed) {
        $this->commentsAllowed = $commentsAllowed;
    }
    public function setCommentCount($commentCount) {
        $this->commentCount = $commentCount;
    }
    public function setPostDate($postDate) {
        $this->postDate = $postDate;
    }
    public function setBlogger($blogger) {
        $this->blogger = $blogger;
    }

    public function display() {
        $blogger = $this->getBlogger();
        $allTags = $this->getTagsAsArray();
        
        echo "<div class='blog-container'>";
        echo "<div class='blog-user-container'>";
        echo "<img src='avatars/". $blogger->getPicture() ."' alt='Avatar' class='blog-avatar'>";
        echo "<span class='blog-username'>" . $blogger->getUserName() . "</span>";
        echo "</div>";
        echo "<p class='blog-body'>" . $this->body . "</p>";
        echo "<p>";
        for ($i = 0; $i < count($allTags); $i++) {
            echo $allTags[$i];
            if ($i != count($allTags) - 1) {
                echo " | ";
            }
        }
        echo "</p>";
        echo "<p>" . $this->commentCount . " Comments</p>";
        echo "<p>" . $this->postDate . "</p>";
        echo "</div>";
    }
    
    public function displayInDetail() {
        $this->display();
        echo "<form method='post' autocomplete='off' action='viewingPost.php'>";
        echo "<input type='hidden' value='" . $this->getId() . "' name='blog'>";
        echo "<input type='submit' value='View'  class='submit-button'>";
        echo "</form>";
    }

}
