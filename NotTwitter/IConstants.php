<?php
/**
 *
 * @author D00181131 - Ronan Duffy
 */
interface IConstants {
    
    const COMMENTS_DISALLOWED = 0;
    
    const MAX_USERNAME_LENGTH = 32;
    
    const MIN_PASSWORD_LENGTH = 6;
    const CONFIRMED_PASSWORD = 0;
     
    const MAX_FILE_SIZE = 250000;
    const FILE_TYPE = "png";
    
    const FIRST_INSTANCE = 0;
}