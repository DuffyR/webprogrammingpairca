<?php
?>
<!DOCTYPE html>
<!--
get user info(name and pic) and use the session to display the current user's name and avatar.
-->
        <header>
            <h1 id="website-title"><a href="home.php">NotTwitter</a></h1>
            <div id ='log'>
            <?php if(isset($_SESSION['userLoggedIn']) == '') {?>
                <p><a href="login.php">Login</a></p>
            <?php } else { ?>
                <?php echo unserialize($_SESSION["userLoggedIn"])->display(); ?>
                <p><a href="logout.php">Logout</a></p>
            <?php } ?>
            </div>
        </header>