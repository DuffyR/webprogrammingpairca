<?php
session_start();

require_once 'inc/PDOConnection.php';
require_once 'User.php';
require_once 'Blog.php';
require_once 'IConstants.php';

$pdo = new PDOConnection();

if (isset($_SESSION["userLoggedIn"]) != "") {
    $loggedInUser = unserialize($_SESSION["userLoggedIn"]);
}

//Gather all blogs from the database
$sql = "SELECT blog.id, blog.body, blog.tags, blog.commentsAllowed, blog.noOfComments as commentCount, blog.postDate, `user`.id as blogger "
        . "FROM blog, `user-blog`, `user` "
        . "WHERE blog.id = `user-blog`.blogID "
        . "AND `user`.id = `user-blog`.userID "
        . "ORDER BY blog.postDate DESC "
        . "LIMIT 30";
$pdo->setStatement($sql);
$blogs = $pdo->query("Blog", array());

//Link each blog with its blogger
$sql = "SELECT * FROM `user` WHERE id = ?";
$pdo->setStatement($sql);
foreach ($blogs as $blog) {
    //Use the userID to find the user
    $bloggerID = $blog->getBlogger();
    //$pdo->execute(array($bloggerID));
    $blog->setBlogger($pdo->query("User", array($bloggerID))[IConstants::FIRST_INSTANCE]);
}

$pdo->close();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <link href="inc/style.css" rel="stylesheet" type="text/css"/>
        <title>Not Twitter</title>
    </head>
    <body>
        <?php include 'header.php'; ?>
        <form method="post" action="searchPost.php" class="form-container">
            <p class="form-title">Search for blogs with:</p>
            <label class="form-title">This user</label>
            <input type="text" name="search-username" class="form-field"><br>
            <label class="form-title">The following words</label>
            <input type="text" name="search-keyword" class="form-field"><br>
            <label class="form-title">The following tag</label>
            <input type="text" name="search-tag" class="form-field"><br>
            <label class="form-title">This creation date</label>
            <input type="text" name="search-date" title="Enter in the format dd-mm-yy" class="form-field"><br>
            <div class="submit-container">
                <input type="submit" value="Search" class="submit-button">
            </div>
        </form>
        <a id="create-post" href="createPost.php"><p>Create a Post</p></a>
        <h2>Recent blogs</h2>
        <?php
        foreach ($blogs as $blog) {
            echo "<div class='blog-on-homepage'>";
            $blog->displayInDetail();
            echo "</div>";
        }
        ?>
        <?php include 'footer.php'; ?>
    </body>
</html>
