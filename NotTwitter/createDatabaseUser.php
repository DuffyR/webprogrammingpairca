<?php

//---Localhost----
//$rootUser = "root";
//$rootPass = "";
//$userName = "NotTwitterUser";
//$userPass = "12345"; //SHA1()
//$host = "localhost";

try {
    $pdo = new PDO("mysql:host=$host", $rootUser, $rootPass);
    $sql = "CREATE USER '$userName'@'$host' "
            . "IDENTIFIED BY '$userPass';"
            . "GRANT SELECT, INSERT, UPDATE, DELETE, CREATE ON *.* TO '$userName'@'$host';"
            . "FLUSH PRIVILEGES;";
    $pdo->exec($sql);
    unset($pdo);
    echo "User created successfully!<br>";
} catch (PDOException $ex) {
    echo "User Create: " . $e->getMessage();
}
