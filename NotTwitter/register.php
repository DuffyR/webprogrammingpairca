<?php
session_start();

require_once 'inc/PDOConnection.php';
require_once 'IConstants.php';

//Don't let people logged in into this page
if (isset($_SESSION["userLoggedIn"]) != "") {
    header("Location: login.php");
    exit();
}

//The following code is from Securimage PHP CAPTCHA
//Source: https://www.phpcaptcha.org/documentation/quickstart-guide/
//Accessed: 29/04/17
include_once $_SERVER["DOCUMENT_ROOT"] . "/NotTwitter/securimage/securimage.php";
$securimage = new Securimage();

$pdo = new PDOConnection();

$error = false;
$errorMsg = "";
//Only consider adding a new user if the form was even submitted
if (isset($_POST["create-user"])) {
    //Write the information from the form into variables
    $name = $_POST["name"];
    $username = $_POST["username"];
    $email = $_POST["email"];
    $password = $_POST["password"];
    $passwordConfirm = $_POST["confirm-password"];
    $avatar = $_FILES["avatar"];
    $captchaValidation = $_POST["captcha-code"];

    //Validate the name
    if (empty($name)) {
        $error = true;
        $errorMsg = "You did not enter a name";
    } else if (!preg_match("/^[a-zA-Z ]+$/", $name)) {
        $error = true;
        $errorMsg = "Only include letters when entering your name";
    }
    //Validate the username
    if (empty($username)) {
        $error = true;
        $errorMsg = "You did not enter a username";
    } else if (strlen($username) > IConstants::MAX_USERNAME_LENGTH) {
        $error = true;
        $errorMsg = "Your username is too long";
    } else if (!preg_match("/^[a-zA-Z0-9]+$/", $username)) {
        $error = true;
        $errorMsg = "There must not be spaces in your username";
    }
    //Check if the username already exists in the database
    $sql = "SELECT COUNT(`userName`) AS `users` FROM `user` WHERE `userName` = '$username'";
    $pdo->setStatement($sql);
    $pdo->execute(array());
    $usernameExists = $pdo->getStatement()->fetch()["users"];
    if ($usernameExists > 0) {
        $error = true;
        $errorMsg = "This username is already in use. Please use a different username.";
    }
    //Validate the email
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $error = true;
        $errorMsg = "Please enter a valid email";
    }
    //Check if the email already exists in the database
    $sql = "SELECT COUNT(`email`) AS `emails` FROM `user` WHERE `email` = '$email'";
    $pdo->setStatement($sql);
    $pdo->execute(array());
    $emailExists = $pdo->getStatement()->fetch()["emails"];
    if ($emailExists > 0) {
        $error = true;
        $errorMsg = "This email is already in use. Please use a different email.";
    }
    //Validate the password
    if (empty($password) || empty($passwordConfirm)) {
        $error = true;
        $errorMsg = "Please enter a password";
    } else if (!preg_match("/[A-Za-z]+/", $password) || !preg_match("/\d+/", $password) || (strlen($password) < IConstants::MIN_PASSWORD_LENGTH)) {
        $error = true;
        $errorMsg = "Your password must be 6 characters or more in length, and have at least one letter & one number";
    } else if (strcmp($password, $passwordConfirm) !== IConstants::CONFIRMED_PASSWORD) {
        $error = true;
        $errorMsg = "Your password must match when confirming it";
    }
    
    //Validate the captcha
    //The following code is from Securimage PHP CAPTCHA
    //Source: https://www.phpcaptcha.org/documentation/quickstart-guide/
    //Accessed: 29/04/17
    if ($securimage->check($captchaValidation) == false) {
        $error = true;
        $errorMsg = "The CAPTCHA code you entered was incorrect";
    }

    //Validate the avatar
    //Using code from this:
    //http://stackoverflow.com/questions/17153624/using-php-to-upload-file-and-add-the-path-to-mysql-database
    //Accessed: 27/04/17
    if (empty($avatar["name"])) {//If they give no image, they get the default
        $fileName = basename("default.png");
    } else {
        $avatar["name"] = strtolower($avatar["name"]);
        $target = "avatars/";
        $target = $target . basename($avatar["name"]);
        $fileName = basename($avatar["name"]);

        if ($avatar["size"] > IConstants::MAX_FILE_SIZE) {
            $error = true;
            $errorMsg = "Your image is too big";
        }
        if (pathinfo($target, PATHINFO_EXTENSION) != IConstants::FILE_TYPE) {
            $error = true;
            $errorMsg = "Your image is must be a PNG";
        }

        //Check if file already exists, to use that instead of uploading it again
        if (!$error) { //But only consider that if there's no errors
            if (!file_exists($target)) {
                if (move_uploaded_file($avatar["tmp_name"], $target)) {
                    $error = false;
                } else {
                    $error = true;
                }
            }
        }
    }


    //If there's no errors after this, add the user
    if (!$error) {
        //This will hash the password
        //Using code from http://forums.devshed.com/php-faqs-stickies-167/program-basic-secure-login-system-using-php-mysql-891201.html
        //Accessed: 27/04/17
        $salt = ""; 
        $salt = hash('sha256', $salt); // result to be stored in the database
        $hash = hash('sha256', $password . $salt);
        $hash = hash('sha256', $hash . $salt); 
        $password = $hash;
        echo $password;

        $sql = "INSERT INTO user(id, name, userName, email, password, picture)"
                . "VALUES(?,?,?,?,?,?)";
        $pdo->setStatement($sql);

        $pArray = array(NULL, $name, $username, $email, $password, $fileName);
        $pdo->execute($pArray);

        unset($name);
        unset($username);
        unset($email);
        unset($password);
        unset($passwordConfirm);
        unset($avatar);
        unset($_POST);
    }
    $pdo->close();
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <link href="inc/style.css" rel="stylesheet" type="text/css"/>
        <title>Not Twitter - Register</title>
    </head>
    <body>
        <?php include 'header.php'; ?>
        <h2>Create user</h2>
        <p id="error-message"><?php echo $errorMsg; ?></p>
        <form method="post" autocomplete="off" enctype="multipart/form-data" class="form-container">
            <label class="form-title">Real Name: </label>
            <input type="text" name="name" class="form-field"><br>
            <label class="form-title">Username: </label>
            <input type="text" name="username" class="form-field"><br>
            <label class="form-title">Email: </label>
            <input type="text" name="email" class="form-field"><br>
            <label class="form-title">Password: </label>
            <input type="password" name="password" class="form-field"><br>
            <label class="form-title">Confirm Password: </label>
            <input type="password" name="confirm-password" class="form-field"><br>
            <!-- Styling the file button -->
            <!-- Source: http://stackoverflow.com/questions/572768/styling-an-input-type-file-button -->
            <!-- Accessed: 29/04/17 -->
            <label class="submit-button" for="avatar" title="Specifying no picture will give you the default avatar">
                <input id="avatar" type="file" name="avatar">
                Choose your avatar
            </label>
            <!------------------------------------------------->
            <br>
            <!-- The following code is from https://www.phpcaptcha.org/documentation/quickstart-guide/-->
            <!-- Accessed: 29/04/17-->
            <div class="captcha-container">
                <img id="captcha" src="securimage/securimage_show.php" alt="CAPTCHA Image">
                <input type="text" name="captcha-code" size="10" maxlength="6" class="form-field"><br>
                <a href="#" onclick="document.getElementById('captcha').src = 'securimage/securimage_show.php?' + Math.random(); return false" class="submit-button">Different Image</a>
            </div>
            <!------------------------------------------------->
            <div class="submit-container">
                <input type="submit" value="Register" name="create-user" class="submit-button">
            </div>
        </form>
        <?php include 'footer.php'; ?>
    </body>
</html>
