<?php

/**
 * Description of PDOConnection
 *
 * @author D00181131 - Ronan Duffy
 */
class PDOConnection {

    private $pdoConnection;
    private $databaseName;
    private $hostName;
    private $userName;
    private $userPassword;
    private $statement;

    function __construct() {
        //-----Localhost-----
//        $this->databaseName = "ca4pair";
//        $this->hostName = "localhost";
//        $this->userName = "NotTwitterUser";
//        $this->userPassword = "12345";
        
        //-----DkIT SQL Server-----
        $this->databaseName = "D00181131";
        $this->hostName = "mysql02host.comp.dkit.ie";
        $this->userName = "D00181131";
        $this->userPassword = "EPtJ#RiX";

        $dsn = "mysql:dbname=$this->databaseName;host=$this->hostName";
        try {
            $this->pdoConnection = new PDO($dsn, $this->userName, $this->userPassword);
            $this->pdoConnection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            echo "Connection creation error:" . $e->getMessage() . PHP_EOL;
            echo "DSN: " . $dsn . PHP_EOL;
        }
    }
    
    function getPdoConnection() {
        return $this->pdoConnection;
    }

    function setStatement($sql) {
        try {
            $this->statement = $this->pdoConnection->prepare($sql);
        } catch (PDOException $e) {
            echo "Set statement error:" . $e->getMessage() . PHP_EOL;
            echo "SQL: " . $sql . PHP_EOL;
        }
    }

    function getStatement() {
        return $this->statement;
    }

    function execute($arrayData) {
        try {
            if (!empty($arrayData) && !is_null($this->statement)) {
                return $this->statement->execute($arrayData);
            }
            if (!is_null($this->statement)) {
                return $this->statement->execute($arrayData);
            }
        } catch (PDOException $e) {
            echo "Execute statement error:" . $e->getMessage() . PHP_EOL;
            echo "Data: " . implode(",", $arrayData) . PHP_EOL;
        }
    }

    function query($className, $arrayData) {
        try {
            if (is_string($className) && !empty($className) && !is_null($this->statement)) {
                $this->statement->execute($arrayData);
                //$this->statement->execute();
                return $this->statement->fetchAll(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, $className);
            }
        } catch (PDOException $e) {
            echo "Execute statement error:" . $e->getMessage() . PHP_EOL;
            echo "Data: " . implode(",", $arrayData) . PHP_EOL;
        }
    }

    function close() {
        try {
            unset($this->pdoConnection);
            unset($this->statement);
        } catch (PDOException $e) {
            echo "Close error:" . $e->getMessage() . PHP_EOL;
        }
    }

}
