<?php

require_once 'IDisplay.php';
require_once 'inc/QuickToString.php';

/**
 * Description of User
 *
 * @author D00181131
 */
class User implements IDisplay{
    use QuickToString;
    
    private $id;
    private $name;
    private $userName;
    private $email;
    private $password;
    private $picture;
    
//    function __construct($id, $name, $userName, $email, $password, $picture) {
//        $this->id = $id;
//        $this->name = $name;
//        $this->userName = $userName;
//        $this->email = $email;
//        $this->password = $password;
//        $this->picture = $picture;
//    }

    public function getId() {
        return $this->id;
    }
    public function getName() {
        return $this->name;
    }
    public function getUserName() {
        return $this->userName;
    }
    public function getEmail() {
        return $this->email;
    }
    public function getPassword() {
        return $this->password;
    }
    public function getPicture() {
        return $this->picture;
    }

    public function setId($id) {
        $this->id = $id;
    }
    public function setName($name) {
        $this->name = $name;
    }
    public function setUserName($userName) {
        $this->userName = $userName;
    }
    public function setEmail($email) {
        $this->email = $email;
    }
    public function setPassword($password) {
        $this->password = $password;
    }
    public function setPicture($picture) {
        $this->picture = $picture;
    }

    public function display() {
        echo "<div class='logged-in-container'>";
        echo "<img src='avatars/". $this->getPicture() ."' alt='Avatar' class='blog-avatar'>";
        echo "<span class='blog-username'>" . $this->getUserName() . "</span>";
        echo "</div>";
    }
    
    public function displayInDetail() {
        $this->display();
    }

}
