<?php

require_once 'IDisplay.php';
require_once 'inc/QuickToString.php';
/**
 * Description of Comment
 *
 * @author D00181131
 */
class Comment implements IDisplay{
    use QuickToString;
    
    private $id;
    private $body;
    private $postDate;
    private $rating;
    private $blogItsOn;
    private $commenter;
    
//    function __construct($id, $body, $postDate, $rating, $blog, $user) {
//        $this->id = $id;
//        $this->body = $body;
//        $this->postDate = $postDate;
//        $this->rating = $rating;
//        $this->blog = $blog;
//        $this->user = $user;
//    }

    public function getId() {
        return $this->id;
    }
    public function getBody() {
        return $this->body;
    }
    public function getPostDate() {
        return $this->postDate;
    }
    public function getRating() {
        return $this->rating;
    }
    public function getBlog() {
        return $this->blogItsOn;
    }
    public function getCommenter() {
        return $this->commenter;
    }

    public function setId($id) {
        $this->id = $id;
    }
    public function setBody($body) {
        $this->body = $body;
    }
    public function setPostDate($postDate) {
        $this->postDate = $postDate;
    }
    public function setRating($rating) {
        $this->rating = $rating;
    }
    public function setBlog($blogItsOn) {
        $this->blogItsOn = $blogItsOn;
    }
    public function setCommenter($commenter) {
        $this->commenter = $commenter;
    }

    public function display() {
        $commenter = $this->getCommenter();
        
        echo "<div class='blog-container'>";
        echo "<div class='blog-user-container'>";
        echo "<img src='avatars/". $commenter->getPicture() ."' alt='Avatar' class='blog-avatar'>";
        echo "<span class='blog-username'>" . $commenter->getUserName() . "</span>";
        echo "</div>";
        echo "<p>" . $this->body . "</p>";
        //echo "<p>" . $this->postDate . "</p>";

        //http://stackoverflow.com/questions/11911448/how-can-i-take-sql-current-timestamp-and-output-as-php-date
        //This link helped me with formatting the postdate
        echo "<p>" . date("d/m/Y", strtotime($this->postDate)) . "</p>";
        //Accessed: 25/04/17
        
        echo "<p>Rating: " . $this->rating;
        echo "<form method='post' action='rate.php'>";
        echo "<input type='hidden' value='" . $this->id . "' name='comment-id'>";
        echo "<input type='submit' value='Dislike' name='rate' class='submit-button'>";
        echo "<input type='submit' value='Like' name='rate' class='submit-button'></form></p>";
        echo "</div>";
    }
    
    public function displayInDetail() {
        $this->display();
    }
}
