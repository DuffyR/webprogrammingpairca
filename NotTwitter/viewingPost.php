<?php
session_start();

require_once 'inc/PDOConnection.php';
require_once 'Blog.php';
require_once 'User.php';
require_once 'Comment.php';
require_once 'IConstants.php';

$pdo = new PDOConnection();


//Retrieve the blog you're viewing in detail
if (isset($_POST["blog"])) {//Is this a post you just made?
    $_SESSION["postIDViewed"] = $_POST["blog"];
    $blogToGet = $_POST["blog"];
} else if (isset($_SESSION["postIDViewed"])) { //Did you click on a blog?
    $blogToGet = $_SESSION["postIDViewed"];
} else {
    header("Location: home.php");
    exit();
}
$sql = "SELECT blog.id, blog.body, blog.tags, blog.commentsAllowed, blog.noOfComments as commentCount, blog.postDate, `user`.id as blogger "
        . "FROM blog, `user-blog`, `user` "
        . "WHERE blog.id = `user-blog`.blogID "
        . "AND `user`.id = `user-blog`.userID "
        . "AND blog.id = ?";
$pdo->setStatement($sql);
$blogInDetail = $pdo->query("Blog", array($blogToGet))[IConstants::FIRST_INSTANCE];

//Retrieve the user that made the blog
$bloggerID = $blogInDetail->getBlogger();
$sql = "SELECT * FROM `user` WHERE id = ?";
$pdo->setStatement($sql);
$blogInDetail->setBlogger($pdo->query("User", array($bloggerID))[IConstants::FIRST_INSTANCE]);

//If the user is submitting a comment,and they are logged in, add it to the blog
if (isset($_POST["your-comment"])) {
    if (isset($_SESSION["userLoggedIn"]) == "") {
        header("Location: login.php");
        exit();
    } else {
        $commenter = unserialize($_SESSION["userLoggedIn"])->getId();
    }

    $sql = "INSERT INTO `comment`(id, body, postDate, rating) "
            . "VALUES (?, ?, ?, ?)";
    $pdo->setStatement($sql);
    $pArray = array(NULL, $_POST["your-comment"], NULL, "DEFAULT");
    $pdo->execute($pArray);
    $lastComment = $pdo->getPdoConnection()->lastInsertId();

    $sql = "INSERT INTO `blog-comments`(commentID, userID, blogID) "
            . "VALUES (?, ?, ?)";
    $pdo->setStatement($sql);
    $pArray = array($lastComment, $commenter, $blogInDetail->getId());
    $pdo->execute($pArray);

    //Update the number the comments the blog has
    $blogInDetail->setCommentCount($blogInDetail->getCommentCount() + 1);
    $howMany = $blogInDetail->getCommentCount();
    $blogID = $blogInDetail->getId();
    $sql = "UPDATE blog SET noOfComments = ? WHERE blog.id = ?";
    $pdo->setStatement($sql);
    $pdo->execute(array($howMany, $blogID));

    unset($_POST["your-comment"]);
}

//Gather all comments that belong to this blog
$sql = "SELECT `comment`.id, `comment`.body, `comment`.postDate, `comment`.rating, blog.id as blogItsOn, `user`.id as commenter "
        . "FROM blog, `user`, `comment`, `blog-comments` "
        . "WHERE blog.id = `blog-comments`.blogID "
        . "AND `user`.id = `blog-comments`.userID "
        . "AND `comment`.id = `blog-comments`.commentID "
        . "AND blog.id = ? "
        . "ORDER BY `comment`.postDate DESC";
$pdo->setStatement($sql);
$comments = $pdo->query("Comment", array($blogToGet));
//Link each comment with its commenter
$sql = "SELECT * FROM `user` WHERE id = ?";
$pdo->setStatement($sql);
foreach ($comments as $comment) {
    $commenterID = $comment->getCommenter();
    $comment->setCommenter($pdo->query("User", array($commenterID))[IConstants::FIRST_INSTANCE]);
}

$pdo->close();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <link href="inc/style.css" rel="stylesheet" type="text/css"/>
        <title>Not Twitter</title>
    </head>
    <body>
        <?php include 'header.php'; ?>
        <?php $blogInDetail->display(); ?>
        <?php if ($blogInDetail->getCommentsAllowed() != IConstants::COMMENTS_DISALLOWED) { ?>
        <form method="post" autocomplete="off" class="form-container">
            <textarea rows="6" cols="40" name="your-comment" class="form-textarea"></textarea>
            <input type="hidden" value="<?php echo $blogInDetail->getId(); ?>" name="blog">
            <input type="submit" value="Post comment" class="submit-button">
        </form>
        <hr>
        <?php foreach ($comments as $comment) {
            $comment->display();
        }?>
        <?php } else {?>
        <hr>
        <p>Comments have been disabled</p>
        <?php }?>
        <?php include 'footer.php'; ?>
    </body>
</html>
