<?php
session_start();

require_once 'inc/PDOConnection.php';

if (isset($_POST["rate"]) == "") {//Remove any people who came here through URL fiddling
    header("Location: home.php");
    exit();
}

$pdo = new PDOConnection();

//Increase or decrease the rating, depending on which was pressed
if (strcmp($_POST["rate"], "Like") == 0) {
    $sql = "UPDATE `comment` SET rating = rating + 1 "
            . "WHERE id = ?";
    $pdo->setStatement($sql);
    $pdo->execute(array($_POST["comment-id"]));
} else if (strcmp($_POST["rate"], "Dislike") == 0) {
    $sql = "UPDATE `comment` SET rating = rating - 1 "
            . "WHERE id = ?";
    $pdo->setStatement($sql);
    $pdo->execute(array($_POST["comment-id"]));
}

$pdo->close();
unset($_POST["rate"]);
header("Location: viewingPost.php");
exit();
?>
