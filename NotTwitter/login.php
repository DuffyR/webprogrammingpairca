<?php
session_start();

require_once 'inc/PDOConnection.php';
require_once 'User.php';
require_once 'IConstants.php';

//Move the user away from this page if they are already logged in?
if (isset($_SESSION["userLoggedIn"]) != "") {
    header("Location: home.php");
    exit();
}

$pdo = new PDOConnection();

$errorMsg = "";

if (isset($_POST["login"])) {
    $username = $_POST["username"];
    $password = $_POST["password"];
     
    //Check if the username exists in the database
    $sql = "SELECT COUNT(`userName`) AS `users` FROM `user` WHERE `userName` = '$username'";
    $pdo->setStatement($sql);
    $pdo->execute(array());
    $usernameExists = $pdo->getStatement()->fetch()["users"];
    if ($usernameExists > 0) {
        $sql = "SELECT * FROM `user` WHERE `userName` = ?";
        $pdo->setStatement($sql);
        $user = $pdo->query("User", array($_POST["username"]))[IConstants::FIRST_INSTANCE];

        //Check the password entered with the password on the database
        $salt = "";
        $salt = hash('sha256', $salt);
        $checkPassword = hash("sha256", $password . $salt);
        $checkPassword = hash("sha256", $checkPassword . $salt);

        if (strcmp($checkPassword, $user->getPassword()) === 0) {
            $_SESSION["userLoggedIn"] = serialize($user);
            header("Location: home.php");
        } else {
            $errorMsg = "Invalid password";
        }
    } else {
        $errorMsg = "Invalid username";
    }
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <link href="inc/style.css" rel="stylesheet" type="text/css"/>
        <title>Not Twitter</title>
    </head>
    <body>
        <?php include 'header.php'; ?>
        <p id="error-message"><?php echo $errorMsg; ?></p>
        <form method="post" autocomplete="off" class="form-container">
            <label class="form-title">Username: </label>
            <input type="text" name="username" class="form-field"><br>
            <label class="form-title">Password: </label>
            <input type="password" name="password" class="form-field"><br>
            <div class="submit-container">
                <input type="submit" value="Login" name="login" class="submit-button"> 
            </div>
        </form>
        <a id="create-post" href="register.php"><p>Register an account</p></a>
        <?php include 'footer.php'; ?>
    </body>
</html>
