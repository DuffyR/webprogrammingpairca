<?php
require_once 'inc/PDOConnection.php';
require_once 'User.php';
require_once 'Blog.php';
require_once 'RegexUtility.php';
require_once 'IConstants.php';

$pdo = new PDOConnection();

$usernameSearch = trim($_POST["search-username"]);
$keywordSearch = trim($_POST["search-keyword"]);
$tagSearch = trim($_POST["search-tag"]);
$dateSearch = trim($_POST["search-date"]);
//Return the user to the home page if no search critera was made
if (empty($usernameSearch) && empty($keywordSearch) && empty($tagSearch) && empty($dateSearch)) {
    header("Location:home.php");
}

$blogsAfterUserSearch = array();
$blogsAfterKeywordSearch = array();
$blogsAfterTagSearch = array();
$blogsAfterFullSearch = array();

//Start off with the base of the sql statement
$sql = "SELECT blog.id, blog.body, blog.tags, blog.commentsAllowed, blog.noOfComments as commentCount, blog.postDate, `user`.id as blogger "
        . "FROM blog, `user-blog`, `user` "
        . "WHERE blog.id = `user-blog`.blogID "
        . "AND `user`.id = `user-blog`.userID ";

//Find out if the person wants to search for specific users
if (!empty($usernameSearch)) {
    $sql .= "AND `user`.userName LIKE '%$usernameSearch%' ";
}

//Gather the relevent blogs from the database
$pdo->setStatement($sql);
$blogsAfterUserSearch = $pdo->query("Blog", array());

//See if the person wanted to search for a keyword
if (!empty($keywordSearch)) {
    foreach ($blogsAfterUserSearch as $blog) {
        if (RegexUtility::detectWord($blog->getBody(), $keywordSearch)) {
            array_push($blogsAfterKeywordSearch, $blog);
        }
    }
} else {
    $blogsAfterKeywordSearch = $blogsAfterUserSearch;
}

//See if the person wanted to search for a tag
if (!empty($tagSearch)) {
    foreach ($blogsAfterKeywordSearch as $blog) {
        if (RegexUtility::detectWordInArray($blog->getTagsAsArray(), $tagSearch)) {
            array_push($blogsAfterTagSearch, $blog);
        }
    }
} else {
    $blogsAfterTagSearch = $blogsAfterKeywordSearch;
}

//See if the person wanted to searh by date
if (!empty($dateSearch)) {
    foreach ($blogsAfterTagSearch as $blog) {
        if (strcmp($dateSearch, date("d-m-y", strtotime($blog->getPostDate()))) == 0) {
            array_push($blogsAfterFullSearch, $blog);
        }
    }
} else {
    $blogsAfterFullSearch = $blogsAfterTagSearch;
}

//Link each blog with its blogger
$sql = "SELECT * FROM `user` WHERE id = ?";
$pdo->setStatement($sql);
foreach ($blogsAfterFullSearch as $blog) {
    //Use the userID to find the user
    $bloggerID = $blog->getBlogger();
    $pdo->execute(array($bloggerID));
    $blog->setBlogger($pdo->getStatement()->fetchAll(PDO::FETCH_CLASS, "User")[IConstants::FIRST_INSTANCE]);
}


$pdo->close();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <link href="inc/style.css" rel="stylesheet" type="text/css"/>
        <title>Not Twitter</title>
    </head>
    <body>
        <?php include 'header.php'; ?>
        <?php
        foreach ($blogsAfterFullSearch as $blog) {
            $blog->displayInDetail();
        }
        ?>
        <?php include 'footer.php'; ?>
    </body>
</html>
