<?php
session_start();

require_once 'inc/PDOConnection.php';
require_once 'User.php';

$pdo = new PDOConnection();

//Don't let people not logged in into this page
if (isset($_SESSION["userLoggedIn"]) == "") {
    header("Location: login.php");
    exit();
}
$loggedInUser = unserialize($_SESSION["userLoggedIn"]);

$error = false;
$errorMsg = "";

//Only consider adding a new blog if the form was even submitted
if (isset($_POST["your-post"])) {
    $postBody = $_POST["your-post"];
    $tags = $_POST["tags"];
    $allowComments = isset($_POST["comments"]) ? true : false;

    if (!$error) {
        //Add the blog into the database
        $sql = "INSERT INTO blog(id, body, tags, commentsAllowed, noOfComments, postDate)"
                . "VALUES(?,?,?,?,?,?)";
        $pdo->setStatement($sql);
        $pArray = array(NULL, $postBody, $tags, $allowComments, 0, NULL);
        $pdo->execute($pArray);
        $postID = $pdo->getPdoConnection()->lastInsertId();

        $sql = "INSERT INTO `user-blog`(blogID, userID)"
                . "VALUES(?,?)";
        $pdo->setStatement($sql);
        $pArray = array($postID, $loggedInUser->getId());
        $pdo->execute($pArray);

        $_SESSION["postIDViewed"] = $postID;
        header("Location: viewingPost.php");

    }
}

$pdo->close();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <link href="inc/style.css" rel="stylesheet" type="text/css"/>
        <title>Not Twitter</title>
    </head>
    <body>

        <?php include 'header.php'; ?>
        <main>
            <form method="post" class="form-container">
                <textarea rows="6" cols="40" name="your-post" class="form-textarea"></textarea><br>
                Enter relevent tags (Seperate with commas)<input type="text" name="tags" autocomplete="off" class="form-field"><br>
                Allow comments<input type="checkbox" name="comments" value="CommentsEnabled" checked><br>
                <div class="submit-container">
                    <input type="submit" value="Post" class="submit-button">
                </div>
            </form>
        </main>
        <?php include 'footer.php'; ?>
    </body>
</html>