<?php

/**
 * Description of RegexUtility
 *
 * @author Ronan
 * 
 * http://stackoverflow.com/questions/9348326/regex-find-word-in-the-string
 * This link helped me with one of the functions here
 * Accessed: 25/04/17
 */
class RegexUtility {
    public static function detectWord($text, $word) {
        $regex = "#\b$word\b#";
        $count = preg_match_all($regex, $text);
        if ($count > 0) {
            return TRUE;
        }
        return FALSE;
    }
    
    public static function detectWordInArray($textArray, $word) {
        $regex = "#\b$word\b#";
        $wordFound = FALSE;
        foreach ($textArray as $text) {
            if (preg_match($regex, $text)) {
                $wordFound = TRUE;
            }
        }
        return $wordFound;
    }
    
    public static function removeFileExtension($file, $extension) {
        $regex = "#(.+)(\.$extension)\b#";
        $replace = "$1";
        
        return preg_replace($regex, $replace, $file);
    }
}
