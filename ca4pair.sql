-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 27, 2017 at 10:36 PM
-- Server version: 10.1.8-MariaDB
-- PHP Version: 5.5.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ca4pair`
--
CREATE DATABASE IF NOT EXISTS `ca4pair` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `ca4pair`;

-- --------------------------------------------------------

--
-- Table structure for table `blog`
--

CREATE TABLE `blog` (
  `id` int(10) UNSIGNED NOT NULL,
  `body` text NOT NULL,
  `tags` varchar(300) NOT NULL,
  `commentsAllowed` tinyint(1) NOT NULL,
  `noOfComments` int(10) NOT NULL,
  `postDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `blog`
--

INSERT INTO `blog` (`id`, `body`, `tags`, `commentsAllowed`, `noOfComments`, `postDate`) VALUES
(1, 'Swiggity swost. Look at my post.', 'look at it,please,look', 1, 2, '2017-04-27 18:03:09'),
(2, 'Hello world!', 'hello,world', 1, 2, '2017-04-27 20:27:21'),
(3, 'There''s only room for one of us, Joe Bloggs', 'Joe Bloggs', 0, 0, '2017-04-27 20:33:56');

-- --------------------------------------------------------

--
-- Table structure for table `blog-comments`
--

CREATE TABLE `blog-comments` (
  `commentID` int(10) UNSIGNED NOT NULL,
  `userID` int(10) UNSIGNED NOT NULL,
  `blogID` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='This is used to reference the comments on a blog';

--
-- Dumping data for table `blog-comments`
--

INSERT INTO `blog-comments` (`commentID`, `userID`, `blogID`) VALUES
(2, 1, 1),
(4, 1, 1),
(5, 1, 2),
(6, 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `comment`
--

CREATE TABLE `comment` (
  `id` int(10) UNSIGNED NOT NULL,
  `body` text NOT NULL,
  `postDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `rating` int(3) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `comment`
--

INSERT INTO `comment` (`id`, `body`, `postDate`, `rating`) VALUES
(2, 'Actually don''t look.', '2017-04-27 18:35:13', -2),
(4, 'Don''t', '2017-04-27 18:50:20', 1),
(5, 'Goodbye world.', '2017-04-27 20:28:15', 20),
(6, 'Why?', '2017-04-27 20:29:27', 0);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) NOT NULL,
  `userName` varchar(32) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `picture` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `name`, `userName`, `email`, `password`, `picture`) VALUES
(1, 'Ronan Duffy', 'Galbenshire', 'Mechabob2@gmail.com', '94886e95aa5b5b0d67ce6f2553c217867e3dafd1c6c034425774abb49f87e1d8', 'default.png'),
(2, 'Joe Bloggs', 'JoBlog32', 'jb87@gmail.com', '5a91adc85740c84ab044dc2263dcac6d3616f26664e078862dc803061e801c1d', 'hell.png'),
(3, 'Blog Joees', 'BlogJo23', 'BJ23@gmail.com', 'f27b87eacd02768e0d384cdae2f2668e64eb91a1cd558a9346f2496b39eb1a17', 'hellnorway.png');

-- --------------------------------------------------------

--
-- Table structure for table `user-blog`
--

CREATE TABLE `user-blog` (
  `blogID` int(10) UNSIGNED NOT NULL,
  `userID` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='This will reference a blog with the user that submitted it';

--
-- Dumping data for table `user-blog`
--

INSERT INTO `user-blog` (`blogID`, `userID`) VALUES
(1, 1),
(2, 2),
(3, 3);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blog-comments`
--
ALTER TABLE `blog-comments`
  ADD PRIMARY KEY (`commentID`,`userID`,`blogID`),
  ADD KEY `userID` (`userID`),
  ADD KEY `blogID` (`blogID`);

--
-- Indexes for table `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user-blog`
--
ALTER TABLE `user-blog`
  ADD PRIMARY KEY (`blogID`,`userID`),
  ADD KEY `userID` (`userID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `blog`
--
ALTER TABLE `blog`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `comment`
--
ALTER TABLE `comment`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `blog-comments`
--
ALTER TABLE `blog-comments`
  ADD CONSTRAINT `blog-comments_ibfk_1` FOREIGN KEY (`commentID`) REFERENCES `comment` (`id`),
  ADD CONSTRAINT `blog-comments_ibfk_2` FOREIGN KEY (`userID`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `blog-comments_ibfk_3` FOREIGN KEY (`blogID`) REFERENCES `blog` (`id`);

--
-- Constraints for table `user-blog`
--
ALTER TABLE `user-blog`
  ADD CONSTRAINT `user-blog_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `user-blog_ibfk_2` FOREIGN KEY (`blogID`) REFERENCES `blog` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
